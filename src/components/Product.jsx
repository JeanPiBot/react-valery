import React from 'react';
//Styles
import '../styles/product.css';

const Product = ({ product, handleAddToBasket }) => {
  return (
    <div className="product-item">
      <img src={product.image} alt={product.title} />
      <div className="product-item-info">
        <h2>
          {product.title}
          <span>
            $ {''} {product.price}
          </span>
        </h2>
        <p>{product.description}</p>
      </div>
      <button type="button" onClick={handleAddToBasket(product)}>
        Agregar al carrito
      </button>
    </div>
  );
};

export default Product;
