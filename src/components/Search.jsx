import React from 'react';
//styles
import '../styles/search.css';

const Search = ({ search, searchInput, handleSearch }) => {
  return (
    <div className="search">
      <div className="search_input">
        <input
          id="search"
          type="text"
          value={search}
          ref={searchInput}
          onChange={handleSearch}
        />
      </div>
      <div className="search_icon">
        <i className="fas fa-search"></i>
      </div>
    </div>
  );
};

export default Search;
