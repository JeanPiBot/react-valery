import React from 'react';
// Components
import Map from '../components/Map';
// Styles
import '../styles/location.css';

const Location = () => {
    return (
        <>
            <div className="location">
                <h1>Localización</h1>
                <p>Te invito a visitarme en mi tienda física ubicada en la dirección: <strong>Calle 13 #11-50, Florencia Caquetá Colombia.</strong></p>
                <div className="location_map">
                    <Map />
                </div>
            </div>
        </>
    )
}

export default Location;
