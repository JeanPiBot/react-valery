import React, { useContext } from 'react';
import { Link } from 'react-router-dom';
//Styles
import '../styles/checkout.css';
// AppContext
import AppContext from '../context/AppContext';
// Handle de la suma total
import handleSumTotal from '../utils/handleSumTotal';

const Checkout = () => {
  const { state, removeFromBasket } = useContext(AppContext);
  const { basket } = state;

  const handleRemove = (product) => () => {
    removeFromBasket(product);
  };

  return (
    <div className="checkout">
      <div className="checkout_content">
        {basket.length > 0 ? <h3>Lista de pedidos:</h3> : <h3>Sin pedidos</h3>}
        {basket.map((item) => (
          <div className="checkout_item">
            <div className="checkout_element">
              <h4>{item.title}</h4>
              <span>{item.price}</span>
            </div>
            <button type="button" onClick={handleRemove(item)}>
              <i className="fas fa-trash-alt"></i>
            </button>
          </div>
        ))}
      </div>
      {basket.length > 0 && (
        <div className="checkout_sibar">
          <h3>{`El precio total es: $ ${handleSumTotal(basket)}`}</h3>
          <Link to="/checkout/information">
            <button type="button">Continuar pedido</button>
          </Link>
        </div>
      )}
    </div>
  );
};

export default Checkout;
